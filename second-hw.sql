drop database if exists things;
create database if not exists things;

use things;

create table if not exists categories(
    id int not null auto_increment primary key,
    name varchar(255) not null,
    description text null
);

create table if not exists location(
    id int not null auto_increment primary key,
    name varchar(255) not null,
    description text null
);

create table if not exists thingsOfAccounting(
    id int not null auto_increment primary key,
    category_id int not null,
    location_id int not null,
    name varchar(255) not null,
    description text null,
    image varchar(255) null,
    constraint thingsOfAccounting_category_id_fk
    foreign key(category_id)
    references categories(id)
    on update cascade
    on delete restrict,
    constraint thingsOfAccounting_location_id_fk
    foreign key(location_id)
    references location(id)
    on update cascade
    on delete restrict
);

insert into categories(name, description)
 values ('Furniture', 'Tables, chairs'),
        ('Computer equipment', 'Computer, printer, Wifi-router'),
        ('Household appliances', 'Cleaner');

insert into location(name, description)
values ('15-room', 'For marketing branch'),
       ('Room of director', 'For Head of company'),
       ('Kitchen', 'Central kitchen');

insert into thingsOfAccounting(category_id, location_id, name, description, image)
values (1, 3, 'Big table', 'For 20 people', null),
       (2, 2, 'M1', 'Mac Book last version', null),
       (3, 1, 'Cleaner', 'In that rood does not have any cleaner', null);

select * from thingsOfAccounting;