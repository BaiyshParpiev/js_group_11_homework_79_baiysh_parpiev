const mysqlDb = require("./mysqlDb");
module.exports = {
    get(table) {
        return mysqlDb.getConnection().query('SELECT id, name from ??', [table]);
    },
    getItem(table, id) {
        return mysqlDb.getConnection().query(
            `SELECT * from ?? where id = ? `, [table, id]
        );

    },
    put(table, info, id) {
        return mysqlDb.getConnection().query(
            'UPDATE ?? set ? where id = ?',
            [table, {...info}, id]
        );
    },
    remove(table, id){
        return mysqlDb.getConnection().query(
            'Delete from ?? where id = ?',
            [table, id]
        )
    }
};