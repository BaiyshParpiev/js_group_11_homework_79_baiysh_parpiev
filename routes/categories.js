const express = require('express');
const mysqlDb = require("../mysqlDb");
const request = require("../request");


const router = express.Router();



router.get('/', async(req, res) => {
    const [categories] = await request.get('categories')
    res.send(categories);
});

router.get('/:id', async(req, res) => {
    const [category] = await request.getItem('categories', req.params.id)
    if(!category){
        return res.status(404).send({error: 'Category not Found'});
    }
    res.send(category[0]);
});

router.post('/',async(req, res) => {
    if(!req.body.name){
        return res.status(404).send({error: 'Date not valid'});
    }

    const category = {
        name: req.body.name,
        description: req.body.description,
    }


    const [newCategory] = await mysqlDb.getConnection().query(
        'INSERT INTO ?? (name, description) values(?, ?)',
        ['categories', category.name, category.description]
    );

    res.send({
        ...category,
        id: newCategory.insertId
    });
});

router.put('/:id', async(req, res) => {
   try{
       if(!req.body.name){
           return res.status(404).send({error: 'Date not valid'});
       }
       const category = {
           name: req.body.name,
           description: req.body.description
       };
       await request.put('location', category, req.params.id)

       res.send({
           ...category,
           id: req.params.id
       });
   }catch(e){
       console.log(e)
   }
});

router.delete('/:id', async(req, res) => {
    try{
        await request.remove('categories', req.params.id)
        res.send('Deleted successful!')
    }catch(e){
        res.send(e.sqlMessage)
    }

});

module.exports = router
