const express = require('express');
const mysqlDb = require("../mysqlDb");
const request = require("../request");


const router = express.Router();

router.get('/', async(req, res) => {
    const [location] = await request.get('location')
    res.send(location);
});

router.get('/:id', async(req, res) => {
    try{
        const [location] = await request.getItem('location', req.params.id)
        if(!location){
            return res.status(404).send({error: 'Location not Found'});
        }
        res.send(location[0]);
    }catch(e){
        console.log(e)
    }
});

router.post('/',async(req, res) => {
    try{
        if(!req.body.name){
            return res.status(404).send({error: 'Date not valid'});
        }
        const location = {
            name: req.body.name,
            description: req.body.description,
        };
        const [newLocation] = await mysqlDb.getConnection().query(
            'INSERT INTO ?? (name, description) values(?, ?)',
            ['location', location.name, location.description]
        );

        res.send({
            ...location,
            id: newLocation.insertId
        });
    }catch(e){
        console.log(e)
    }
});

router.put('/:id', async(req, res) => {
    if(!req.body.name){
        return res.status(404).send({error: 'Date not valid'});
    }
    const location = {
        name: req.body.name,
        description: req.body.description
    };
    await request.put('location', location, req.params.id)
    res.send({
        ...location,
        id: req.params.id
    });

});

router.delete('/:id', async(req, res) => {
    try{
        await request.remove('location', req.params.id)
        res.send('Deleted successful!')
    }catch(e){
        res.send(e.sqlMessage)
    }

});

module.exports = router
