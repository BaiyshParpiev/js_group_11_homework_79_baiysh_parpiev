const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid')
const config = require('../config');
const mysqlDb = require("../mysqlDb");
const request = require('../request');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});


const upload = multer({storage});
const router = express.Router();



router.get('/', async(req, res) => {
    const [resources] = await request.get('thingsOfAccounting');
    res.send(resources);
});

router.get('/:id', async(req, res) => {
    try{
        const [resource] = await request.getItem('thingsOfAccounting', req.params.id);
        if(!resource){
            return res.status(404).send({error: 'Resource not Found'});
        }
        res.send(resource[0]);
    }catch (e){
        console.log(e)
    }
});

router.post('/', upload.single('image'), async(req, res) => {
   try{
       if(!req.body.name || !req.body.category_id || !req.body.location_id){
           return res.status(404).send({error: 'Date not valid'});
       }

       const resource = {
           name: req.body.name,
           location_id: req.body.location_id,
           category_id: req.body.category_id,
           description: req.body.description,
       }

       if(req.file){
           resource.image = req.file.filename;
       }

       const [newProduct] = await mysqlDb.getConnection().query(
           'INSERT INTO ?? (category_id, location_id, name, description, image) values(?, ?, ?, ?, ?)',
           ['thingsOfAccounting',resource.category_id, resource.location_id, resource.name, resource.description, resource.image]
       );

       res.send({
           ...resource,
           id: newProduct.insertId
       });
   }catch(e){
       console.log(e)
   }
});

router.put('/:id', upload.single('image'), async(req, res) => {
   try{
       const resource = {
           category_id: req.body.category_id,
           location_id: req.body.location_id,
           name: req.body.name,
           description: req.body.description,
       };

       if(req.file) resource.image = req.file.filename;

       await request.put('thingsOfAccounting', resource, req.params.id);

       res.send({
           ...resource,
           id: req.params.id
       });
   }catch (e){
       console.log(e)
   }
});

router.delete('/:id', async(req, res) => {
    try{
        await request.remove('thingsOfAccounting', req.params.id)
        res.send('Deleted successful!')
    }catch(e){
        res.status(401).send(e.sqlMessage)
    }
});

module.exports = router
