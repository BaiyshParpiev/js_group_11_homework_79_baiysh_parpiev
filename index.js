const express = require('express');
const mysqlDb = require('./mysqlDb');
const locations = require('./routes/locations');
const categories = require('./routes/categories');
const resources = require('./routes/resources');

const app = express();

app.use(express.json());
app.use(express.static('public'));

app.use('/locations', locations)
app.use('/categories', categories)
app.use('/resources', resources)

const port = 8000;

mysqlDb.connect().catch(e => console.log(e))
app.listen(port, () => {
    console.log('Server is started successful on port', port)
})